package com.aeditip.banco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoTallerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoTallerApplication.class, args);
	}

}
