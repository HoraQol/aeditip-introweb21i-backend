package com.aeditip.banco.entidades.cliente.ubigeo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.entidades.cliente.PersonalCliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@SuppressWarnings("serial")
public class LlaveUbigeo implements Serializable {
	@Column(name="pais")
	private String pais;
	
	@Column(name="codigo")
	private String codigo;
}
