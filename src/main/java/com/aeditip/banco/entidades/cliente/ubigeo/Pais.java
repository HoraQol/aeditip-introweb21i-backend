package com.aeditip.banco.entidades.cliente.ubigeo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.entidades.cliente.PersonalCliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="pais")
public class Pais {
	@Id
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="numeracion")
	private int numeracion;
}
