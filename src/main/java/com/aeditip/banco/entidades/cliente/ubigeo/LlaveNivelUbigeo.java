package com.aeditip.banco.entidades.cliente.ubigeo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@SuppressWarnings("serial")
public class LlaveNivelUbigeo implements Serializable {
	@Column(name="pais")
	private String pais;
	
	@Column(name="nivel")
	private String nivel;
}
