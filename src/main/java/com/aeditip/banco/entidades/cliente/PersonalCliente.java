package com.aeditip.banco.entidades.cliente;

import javax.persistence.Embeddable;

import com.aeditip.banco.entidades.Cliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class PersonalCliente {
	private String documento;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String prenombres;
	
	public String armarNombreCompleto() {
		return apellidoPaterno.toUpperCase() + " " + apellidoMaterno.toUpperCase()
				+ ", " + prenombres;
	}
}
