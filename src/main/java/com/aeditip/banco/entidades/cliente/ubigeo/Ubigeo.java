package com.aeditip.banco.entidades.cliente.ubigeo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.entidades.cliente.PersonalCliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ubigeo")
public class Ubigeo {
	@EmbeddedId
	private LlaveUbigeo codigo;
	
	@ManyToOne
	@MapsId("pais")
	@JoinColumn(name = "pais")
	private Pais pais;
	
	@Column(name = "nombre")
	private String nombre;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="pais", referencedColumnName="pais",
				insertable=false, updatable=false),
		@JoinColumn(name="nivel", referencedColumnName="nivel",
				insertable=false, updatable=false)
	})
	private NivelUbigeo nivel;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumns({
		@JoinColumn(name="pais", referencedColumnName="pais",
				insertable=false, updatable=false),
		@JoinColumn(name="superior", referencedColumnName="codigo",
				insertable=false, updatable=false)
	})
	private Ubigeo superior;
}
