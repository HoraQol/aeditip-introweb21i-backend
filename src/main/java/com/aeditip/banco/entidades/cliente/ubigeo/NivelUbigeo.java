package com.aeditip.banco.entidades.cliente.ubigeo;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="nivel_ubigeo")
public class NivelUbigeo {
	@EmbeddedId
	private LlaveNivelUbigeo llave;

	@ManyToOne
	@MapsId("pais")
	@JoinColumn(name = "pais")
	private Pais pais;
	
	@Column(name = "nombre")
	private String nombre;
}
