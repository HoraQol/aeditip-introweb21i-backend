package com.aeditip.banco.entidades;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="transferencia")
public class Transferencia {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="cuenta_origen")
	private CuentaBancaria origen;
	
	@ManyToOne
	@JoinColumn(name="cuenta_destino")
	private CuentaBancaria destino;
	
	@Column(name="monto")
	private double monto;
	
	@Column(name="referencia")
	private String referencia;
	
	@Column(name="fecha_operacion")
	private LocalDateTime fechaCreacion;
}
