package com.aeditip.banco.entidades;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="cuenta_bancaria")
public class CuentaBancaria {
	@Id
	@Column(name="numero")
	private String numero;
	
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;
	
	private int idTipo;
	
	@Column(name="saldo")
	private double saldo;
	
	@Column(name="fecha_creacion")
	private LocalDate fechaCreacion;
}
