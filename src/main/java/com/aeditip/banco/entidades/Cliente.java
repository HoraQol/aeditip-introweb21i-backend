package com.aeditip.banco.entidades;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.aeditip.banco.entidades.cliente.PersonalCliente;
import com.aeditip.banco.entidades.cliente.ubigeo.Ubigeo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="cliente")
public class Cliente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	/*@Column(name="documento_identidad")
	private String documento;
	
	@Column(name="apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name="apellido_materno")
	private String apellidoMaterno;
	
	@Column(name="prenombres")
	private String prenombres;*/
	
	@Embedded
	@AttributeOverrides(value= {
			@AttributeOverride( name="documento", column=@Column(name="documento_identidad")),
			@AttributeOverride( name="apellidoMaterno", column=@Column(name="apellido_paterno")),
			@AttributeOverride( name="apellidoPaterno", column=@Column(name="apellido_materno")),
			@AttributeOverride( name="prenombres", column=@Column(name="prenombres"))
	})
	private PersonalCliente personal;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="pais_ubigeo", referencedColumnName="pais"),
		@JoinColumn(name="codigo_ubigeo", referencedColumnName="codigo")
	})
	private Ubigeo ubigeo;
	
	@Column(name="correo_electronico")
	private String correoElectronico;
	
	@Column(name="direccion")
	private String direccion;
	
	@Column(name="contrasena")
	private String contrasena;
}
