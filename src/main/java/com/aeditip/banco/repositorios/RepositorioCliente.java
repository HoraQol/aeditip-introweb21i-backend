package com.aeditip.banco.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aeditip.banco.entidades.Cliente;

@Repository
public interface RepositorioCliente extends JpaRepository<Cliente, Integer> {
	@Query(value="SELECT * FROM cliente WHERE correo_electronico = ?1", nativeQuery=true)
	Optional<Cliente> verificarCorreo(String correo);
}
