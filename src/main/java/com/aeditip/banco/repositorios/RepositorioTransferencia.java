package com.aeditip.banco.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aeditip.banco.entidades.Transferencia;

@Repository
public interface RepositorioTransferencia extends JpaRepository<Transferencia, Integer>{
	@Query(value="SELECT * FROM transferencia WHERE cuenta_origen=?1 OR cuenta_destino=?1 "
			+ "ORDER BY fecha_operacion DESC", nativeQuery=true)
	List<Transferencia> obtenerHistorialdeCuenta(String numeroCuenta);
}
