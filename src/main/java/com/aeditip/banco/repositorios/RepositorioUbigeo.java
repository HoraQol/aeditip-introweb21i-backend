package com.aeditip.banco.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.banco.entidades.cliente.ubigeo.LlaveUbigeo;
import com.aeditip.banco.entidades.cliente.ubigeo.Ubigeo;

@Repository
public interface RepositorioUbigeo extends JpaRepository<Ubigeo, LlaveUbigeo>{
}
