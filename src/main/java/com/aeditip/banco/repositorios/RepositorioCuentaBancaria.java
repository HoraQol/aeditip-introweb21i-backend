package com.aeditip.banco.repositorios;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.entidades.CuentaBancaria;

@Repository
public interface RepositorioCuentaBancaria extends JpaRepository<CuentaBancaria, String>{
	@Query(value="SELECT B.* FROM cuenta_bancaria B, cliente C WHERE " 
			+ "B.id_cliente = C.id AND C.correo_electronico = ?1 "
			+ "LIMIT 1", nativeQuery=true)
	Optional<CuentaBancaria> obtenerCuentaPrimariaCliente(String correo);
}
