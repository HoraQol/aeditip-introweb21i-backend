package com.aeditip.banco.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.banco.dto.DTOCliente;
import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.servicios.ServicioCliente;

@RestController
@RequestMapping("/api/cliente")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST})
public class ControladorCliente {
	@Autowired
	private ServicioCliente servCliente;
	
	//GET http://localhost:8081/api/cliente/listar
	@RequestMapping(value = "listar", method = RequestMethod.GET)
	public List<DTOCliente> listarClientes(){
		return servCliente.listarClientes();
	}
	
	//POST http://localhost:8081/api/cliente/crear
	@RequestMapping(value = "crear", method = RequestMethod.POST)
	public int crearCliente(@RequestBody DTOCliente cliente){
		return servCliente.crearCliente(cliente);
	}
	
	/* En estos saldrá un error en relación a la serialización
	 * Esto lo veremos mañana
	 */
	
	//GET http://localhost:8081/api/cliente?id=1
	/*@RequestMapping(value = "", method = RequestMethod.GET)
	public Cliente obtenerCliente(@RequestParam int id) {
		return servCliente.obtenerCliente(id);
	}*/
	
	//GET http://localhost:8081/api/cliente/1
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public DTOCliente obtenerCliente(@PathVariable int id) {
		return servCliente.obtenerCliente(id);
	}
}
