package com.aeditip.banco.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.banco.dto.DTOTransferencia;
import com.aeditip.banco.servicios.ServicioCuentaBancaria;
import com.aeditip.banco.servicios.ServicioTransferencia;

@RestController
@RequestMapping("/api/transferencia")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST})
public class ControladorTransferencia {
	@Autowired
	private ServicioCuentaBancaria servCuenta;
	@Autowired
	private ServicioTransferencia servTransferencia;
	
	@RequestMapping(value = "validar", method = RequestMethod.GET)
	public String validarCuenta(@RequestParam String numeroCuenta) {
		return servCuenta.validarCuenta(numeroCuenta);
	}
	
	// GET http://localhost:8081/api/transferencia/historial/{N°}
	@RequestMapping(value = "historial/{numeroCuenta}", method = RequestMethod.GET)
	public List<DTOTransferencia> obtenerHistorial(@PathVariable String numeroCuenta) {
		return servTransferencia.obtenerHistorialCuenta(numeroCuenta);
	}
	
	@RequestMapping(value = "realizar-transferencia", method = RequestMethod.POST)
	public int realizarTransferencia(@RequestBody DTOTransferencia transferencia){
		return servTransferencia.realizarTransferencia(transferencia);
	}
}
