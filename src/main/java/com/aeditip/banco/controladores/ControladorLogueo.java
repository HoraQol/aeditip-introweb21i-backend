package com.aeditip.banco.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.banco.dto.DTOClaveValor;
import com.aeditip.banco.dto.DTOLogueo;
import com.aeditip.banco.dto.DTOPerfil;
import com.aeditip.banco.dto.DTORespuesta;
import com.aeditip.banco.servicios.ServicioCliente;
import com.aeditip.banco.servicios.ServicioCuentaBancaria;
import com.aeditip.banco.servicios.ServicioTransferencia;

@RestController
@CrossOrigin(origins="*",methods= {RequestMethod.POST})
public class ControladorLogueo {
	@Autowired
	private ServicioCliente servCliente;
	@Autowired
	private ServicioCuentaBancaria servBancaria;
	@Autowired
	private ServicioTransferencia servTransferencia;
	
	@RequestMapping(value = "logueo", method = RequestMethod.POST)
	public DTORespuesta<DTOPerfil> loguear(@RequestBody DTOLogueo credenciales) {
		DTOPerfil perfil = new DTOPerfil();
		String nombre = servCliente.autenticar(credenciales);
		 if(nombre.equals(""))
			 return new DTORespuesta<DTOPerfil>(false, null);
		 else {
			 perfil.setNombre(nombre);
			 DTOClaveValor<Double> cuenta = servBancaria.obtenerDatos(credenciales.getUsuario());
			 perfil.setSaldo(cuenta.getClave());
			 perfil.setTransferencias(servTransferencia.obtenerHistorialCuenta(cuenta.getValor()));
			 return new DTORespuesta<DTOPerfil>(true, perfil);
		 }
	}
}
