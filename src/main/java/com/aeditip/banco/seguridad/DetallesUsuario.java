package com.aeditip.banco.seguridad;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.repositorios.RepositorioCliente;

@Service
public class DetallesUsuario implements UserDetailsService {
	@Autowired
	private RepositorioCliente repoCliente;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Optional<Cliente> cliente = repoCliente.verificarCorreo(userName);
		
		return new Usuario(cliente.get());
	}
}
