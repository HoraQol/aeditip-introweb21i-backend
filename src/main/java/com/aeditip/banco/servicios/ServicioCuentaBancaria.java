package com.aeditip.banco.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.banco.dto.DTOClaveValor;
import com.aeditip.banco.dto.DTOTransferencia;
import com.aeditip.banco.entidades.CuentaBancaria;
import com.aeditip.banco.repositorios.RepositorioCuentaBancaria;

@Service
public class ServicioCuentaBancaria {
	@Autowired
	private RepositorioCuentaBancaria repoCuenta;
	
	public String validarCuenta(String numero) {
		CuentaBancaria cuenta = repoCuenta.getById(numero);
		return cuenta == null ? "" :
				cuenta.getCliente().getPersonal().armarNombreCompleto();
	}
	
	public DTOClaveValor<Double> obtenerDatos(String correo) {
		CuentaBancaria cuenta = repoCuenta.obtenerCuentaPrimariaCliente(correo).orElse(null);
		return cuenta == null ? new DTOClaveValor<Double>(0.0, ""): 
			new DTOClaveValor<Double>(cuenta.getSaldo(), cuenta.getNumero());
	}
}
