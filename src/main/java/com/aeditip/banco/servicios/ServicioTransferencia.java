package com.aeditip.banco.servicios;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.banco.dto.DTOTransferencia;
import com.aeditip.banco.entidades.CuentaBancaria;
import com.aeditip.banco.entidades.Transferencia;
import com.aeditip.banco.errores.personalizada.SaldoInsuficienteEx;
import com.aeditip.banco.repositorios.RepositorioCuentaBancaria;
import com.aeditip.banco.repositorios.RepositorioTransferencia;

@Service
public class ServicioTransferencia {
	@Autowired
	private RepositorioTransferencia repoTransferencia;
	@Autowired
	private RepositorioCuentaBancaria repoCuenta;
	
	public Transferencia traductor(DTOTransferencia tsf) {
		Transferencia auxTsf = new Transferencia();
		CuentaBancaria origen = repoCuenta.getById(tsf.getOrigen());
		if(origen.getSaldo() < tsf.getMonto())
			throw new SaldoInsuficienteEx();
		auxTsf.setOrigen(origen);
		auxTsf.setDestino(repoCuenta.getById(tsf.getDestino()));
		auxTsf.setMonto(tsf.getMonto());
		auxTsf.setReferencia(tsf.getReferencia());
		auxTsf.setFechaCreacion(LocalDateTime.now());
		return auxTsf;
	}
	
	public int realizarTransferencia(DTOTransferencia tsf) {
		return repoTransferencia.save(traductor(tsf)).getId();
	}
	
	public List<DTOTransferencia> obtenerHistorialCuenta(String numeroCuenta){
		return repoTransferencia.obtenerHistorialdeCuenta(numeroCuenta)
				.stream().map(tsf -> new DTOTransferencia(tsf))
				.collect(Collectors.toList());
	}
}
