package com.aeditip.banco.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.aeditip.banco.dto.DTOCliente;
import com.aeditip.banco.dto.DTOLogueo;
import com.aeditip.banco.entidades.Cliente;
import com.aeditip.banco.entidades.cliente.PersonalCliente;
import com.aeditip.banco.entidades.cliente.ubigeo.LlaveUbigeo;
import com.aeditip.banco.entidades.cliente.ubigeo.Ubigeo;
import com.aeditip.banco.errores.UbigeoNFEx;
import com.aeditip.banco.errores.harex.CorreoHAREx;
import com.aeditip.banco.repositorios.RepositorioCliente;
import com.aeditip.banco.repositorios.RepositorioUbigeo;

@Service
public class ServicioCliente {
	@Autowired
	private RepositorioCliente repoCliente;
	@Autowired
	private RepositorioUbigeo repoUbigeo;
	
	public Cliente traductor(DTOCliente mascara) {
		Cliente cli = new Cliente();
		// Opción 1
		/*PersonalCliente personal = new PersonalCliente();
		personal.setDocumento(mascara.getDni());
		personal.setApellidoPaterno(mascara.getApellidoPaterno());
		personal.setApellidoMaterno(mascara.getApellidoMaterno());
		personal.setPrenombres(mascara.getPrenombres());
		cli.setPersonal(personal);*/
		// Opción del Taller
		cli.setPersonal(new PersonalCliente(mascara.getDni(), mascara.getApellidoPaterno(),
				mascara.getApellidoMaterno(), mascara.getPrenombres()));
		// Ubigeo
		String[] datosUbigeo = mascara.getUbigeo().split("-");
		Ubigeo ubig = repoUbigeo.getById(new LlaveUbigeo(datosUbigeo[0], 
				datosUbigeo[1]));
		if(ubig == null)
			throw new UbigeoNFEx(mascara.getUbigeo());
		cli.setUbigeo(ubig);
		cli.setCorreoElectronico(mascara.getCorreo());
		if(repoCliente.verificarCorreo(mascara.getCorreo()).isPresent())
			throw new CorreoHAREx(mascara.getCorreo());
		cli.setDireccion(mascara.getDireccion());
		cli.setContrasena(mascara.getContrasena());
		return cli;
	}
	
	
	public List<DTOCliente> listarClientes(){
		/*List<Cliente> clis = repoCliente.findAll();
		List<DTOCliente> auxClis = new ArrayList<DTOCliente>();
		
		for(Cliente c : clis) {
			auxClis.add(new DTOCliente(c));
		}
		
		return auxClis;*/
		
		return repoCliente.findAll().stream()
				.map(c -> new DTOCliente(c))
				.collect(Collectors.toList());
	}
	
	public int crearCliente(DTOCliente cliente) {
		return repoCliente.save(traductor(cliente)).getId();
	}
	
	public DTOCliente obtenerCliente(int idCliente) {
		return new DTOCliente(repoCliente.getById(idCliente));
	}
	
	public String autenticar(DTOLogueo credenciales) {
		Cliente cli = repoCliente.verificarCorreo(credenciales.getUsuario()).orElse(null);
		return (cli != null && 
				new BCryptPasswordEncoder().matches(credenciales.getClave(), cli.getContrasena()))
				? cli.getPersonal().armarNombreCompleto() : "";
	}
}
