package com.aeditip.banco.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DTOLogueo implements Serializable {
	private String usuario;
	private String clave;
}
