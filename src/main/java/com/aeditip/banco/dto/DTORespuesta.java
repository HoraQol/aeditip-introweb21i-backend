package com.aeditip.banco.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DTORespuesta<T> implements Serializable {
	private Boolean exito;
	private T valor;
}
