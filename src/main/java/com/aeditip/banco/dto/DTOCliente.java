package com.aeditip.banco.dto;

import java.io.Serializable;

import com.aeditip.banco.entidades.Cliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DTOCliente implements Serializable {
	private int id;
	private String dni;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String prenombres;
	private String ubigeo; // PER-030607
	private String correo;
	private String direccion;
	private String contrasena;
	
	public DTOCliente(Cliente cli) {
		this.id = cli.getId();
		this.dni = cli.getPersonal().getDocumento();
		this.apellidoPaterno = cli.getPersonal().getApellidoPaterno();
		this.apellidoMaterno = cli.getPersonal().getApellidoMaterno();
		this.prenombres = cli.getPersonal().getPrenombres();
		this.ubigeo = cli.getUbigeo().getPais().getCodigo() + "-" +
				cli.getUbigeo().getCodigo().getCodigo();
		this.correo = cli.getCorreoElectronico();
		this.direccion = cli.getDireccion();
	}
}
