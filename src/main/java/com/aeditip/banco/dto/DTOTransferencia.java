package com.aeditip.banco.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.aeditip.banco.entidades.Transferencia;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DTOTransferencia implements Serializable {
	public int id;
	public String origen;
	public String destino;
	public double monto;
	public String referencia;
	@JsonDeserialize(using=LocalDateTimeDeserializer.class)
	@JsonSerialize(using=LocalDateTimeSerializer.class)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public LocalDateTime fecha;
	
	public DTOTransferencia(Transferencia tsf) {
		this.id = tsf.getId();
		this.origen = tsf.getOrigen().getNumero();
		this.destino = tsf.getDestino().getNumero();
		this.monto = tsf.getMonto();
		this.fecha = tsf.getFechaCreacion();
		this.referencia = tsf.getReferencia();
	}
}
