package com.aeditip.banco.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DTOPerfil implements Serializable  {
	private String nombre;
	private double saldo;
	private List<DTOTransferencia> transferencias;
}
