package com.aeditip.banco.errores.harex;

@SuppressWarnings("serial")
public class CorreoHAREx extends RuntimeException {
	public CorreoHAREx(String ubigeo) {
		super("No puede crear un cliente con el correo " + ubigeo + 
				". Este ya ha sido registrado.");
	}
}
