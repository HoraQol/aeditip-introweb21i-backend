package com.aeditip.banco.errores;

@SuppressWarnings("serial")
public class UbigeoNFEx extends RuntimeException {
	public UbigeoNFEx(String ubigeo) {
		super("No se ha encontrado el Ubigeo " + ubigeo);
	}
}
