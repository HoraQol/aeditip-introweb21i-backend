package com.aeditip.banco.errores.personalizada;

@SuppressWarnings("serial")
public class SaldoInsuficienteEx extends RuntimeException {
	public SaldoInsuficienteEx() {
		super("La cuenta de origen no tiene saldo suficiente para proceder "
				+ "con la transferencia");
	}
}
