# Taller de Introducción al Desarrollo Web 2021-I - Proyecto Backend

¡Bienvenido al Repositorio Backend del Taller de Introducción al Desarrollo Web, en su edición 2021-I! Aquí se alojan los archivos del proyecto en Spring Boot que se realice en las sesiones del Taller. Este proyecto resuelve el Problema de Aplicación propuesto para el Taller, tanto en sus versiones básica como sus variables. De parte del Grupo AEDITIP y de la Plana Docente, agradecemos su visita a este repositorio y esperamos que el contenido de este.

# Tabla de Contenido
1. [Sobre el Taller](#sobre)
2. [Docentes](#docentes)
    1. [Historia](#historia)
3. [Ramas del Repositorio](#ramas)
4. [Indicaciones](#indicaciones)
5. [Consultas](#consultas)
6. [Agradecimientos especiales](#gracias)
7. [Contacto](#contacto)

## Sobre el Taller <a name="sobre"></a>

El Taller de Introducción al Desarrollo Web, en su edición 2021-I es organizado por el [Grupo AEDITIP](https://www.linkedin.com/company/aeditip/) y la [Sociedad de Apoyo en Informática](https://www.facebook.com/sai.pucp/). El taller consta de seis (06) sesiones, las cuales fueron realizadas del 14 al 20 de agosto de 2021. Para más información, puede revisar el temario oficial del Taller a través de }[este enlace](https://bit.ly/3jplntw).

### Historia <a name="historia"></a>

Este taller representa la primera actividad académica netamente desarrollada por el Grupo AEDITIP luego de su cierre temporal, tras una serie de señalamientos injustificados por parte de autoridades en una especialidad de la Pontificia Universidad Católica del Perú (PUCP).

A inicios de agosto de 2021, el Grupo AEDITIP planteó la posibilidad de realizar un taller, pensando en alumnos que necesitan aprender desarrollo web (a menos utilizando algunos de las tecnologías más demandadas en el mercado laboral tecnológico) para llevar algún curso que lo solicite (no obligatoriamente, pero recomendablemente). También, este taller está pensado para los profesionales que están interesados en aprender tanto React como Spring Boot, para ampliar su catálogo de conocimientos con estas tecnologías.

## Docentes <a name="docentes"></a>

- [Sr. José Serrano Amaut](https://www.linkedin.com/in/jos%C3%A9-manuel-serrano-amaut-794549119/): Estudiante de la Pontificia Universidad Católica del Perú (PUCP). Docente de los Tópicos de Desarrollo Frontend.
- [B.Sc. Jorge Fatama Vera](https://www.linkedin.com/in/jfatamav/): Egresado de la Pontificia Universidad Católica del Perú (PUCP). Co-fundador y Director del Grupo AEDITIP. Docente de los Tópicos de Desarrollo Backend.
- Sr. Josué Llaque Agramonte: Estudiante de la Pontificia Universidad Católica del Perú (PUCP). Docente asistente de los Tópicos de Desarrollo Frontend.

## Ramas del Repositorio <a name="ramas"></a>

|  Nombre | Descripción | Disponible a partir de |
| ------- | ----------- | ---------------------- |
| main | Esta rama. Versión estable | 2021/08/17 |
| develop | Versión en desarrollo | 2021/08/17 |

## Indicaciones <a name="indicaciones"></a>

- Este proyecto tendrá dos ramas: en "main" se subirán los cambios estables; en "develop", los cambios que aún se están implementando y están sujetos a cambios.
- Es probable que, en base a las sugerencias obtenidas en nuestros ciclos de mejora continua, se realicen actualizaciones, por lo que el contenido del repositorio está sujeto a cambios. Puede ver el historial de modificaciones en el detalle de la rama.
- Puede revisar [la carpeta del Taller en Google Drive](https://bit.ly/3m6ELhS) para revisar los documentos oficiales del Taller. Al igual que las ramas de este repositorio, los documentos allí alojados están sujetos a modificaciones.

## ¿Te perdiste el Taller? <a name="lo-perdiste"></a>

¿Recién escuchas sobre este taller?, ¿quisiste inscribirte, pero las inscripciones están cerradas?, ¿te inscribiste pero no pudiste asistir a todas las sesiones? ¡No te preocupes! El Grupo AEDITIP habilitará las grabaciones de las sesiones en [su canal de YouTube](https://academiatemple.com/course/?id=yPQWte2eFM8nyzpojLNW). Estos vídeos pasarán por un proceso de edición y renderización antes de su publicación, así que te pedimos paciencia.

## Consultas <a name="consultas"></a>

Si tiene alguna duda, sugerencia o interrogante, no dude en realizarla a los correos [a20122128@pucp.pe](mailto:a20122128@pucp.pe?subject=[introweb21i]%20Consulta), [jorge.fatama@pucp.edu.pe](mailto:jorge.fatama@pucp.edu.pe?subject=[introweb21i]%20Consulta) o [josue.llaque@pucp.edu.pe](mailto:josue.llaque@pucp.edu.pe?subject=[introweb21i]%20Consulta). Cualquier sugerencia o consulta, brindada de manera respetuosa, será bienvenida y será tomada en cuenta para los ciclos de mejora continua que ejecutamos constantemente; desde ya, brindamos un agradecimiento de antemano por su colaboración.

## Certificación y Material de Pago <a name="certificacion"></a>

La Plana Docente desarrolló un material de ejemplo con un proyecto (tanto frontend como backend); podrás adquirirlo a un módico precio realizando un pago a los métodos de pago del Grupo. Asimismo, si te perdiste el Taller y aún deseas adquirir el Certificado, tendrás que realizar los pagos y rendir un Examen de Evaluación de Conocimientos, el cual deberá aprobar con una calificación en escala vigesimal mínima de catorce (14). Más información pronto.

**Nota:** Si asististe al mínimo de sesiones para el Certificado recibirás este documento automaticamente, sin necesidad de examen.

## Agradecimientos especiales <a name="gracias"></a>

Un agradecimiento a cada uno de los alumnos asistentes y a los alumnos que propusieron ideas y sugerencias para la mejora. Un agradecimiento especial a cada uno de los alumnos que adquirieron su derecho a emisión del Certificado; su contribución ayudará a que el Grupo AEDITIP siga realizando más talleres y difundiendo conocimiento.

## Contacto <a name="contacto"></a>

Por favor, sigan nuestras redes sociales para estar al tanto de nuestros Talleres futuros y novedades (haga clic en los enlaces de la lista siguiente para acceder).
- [Página de Facebook.](https://www.facebook.com/aeditip)
- [Perfil de LinkedIn.](https://www.linkedin.com/company/aeditip/)
- [Perfil de Instagram.](https://www.instagram.com/aeditip/)
- [Canal de YouTube.](https://www.youtube.com/channel/UCdJRJR44iAbEl1cTN1-hhhA)
- [Servidor Educativo de Discord.](https://discord.gg/XK7P8EaVvQ)

Asimismo, recomendamos seguir la [página de Facebook de la Soc. de Apoyo en Informática](https://www.facebook.com/sai.pucp/)
